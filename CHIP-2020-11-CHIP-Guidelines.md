```
Title: CHIP Guidelines  
Created: 2020-11-20  
Last Edited: 2021-06-3
Type: Operational  
Status: Draft  
Version: 1.0
```

# CHIP Guidelines

### License

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

### What is a CHIP?

CHIP stands for CasH Improvement Proposal. It is a public document that outlines a proposed change for BCH. It shall include descriptions of the proposal itself, elaboration of its risk/reward, and any relevant technical aspects. Authoring a CHIP is only one of many step towards getting a proposal into BCH.

### CHIP Types

- An Informational CHIP describes a Bitcoin Cash design subject or provides general guidelines and information to the Bitcoin Cash community but does not represent a change to the Bitcoin Cash protocol nor does it affect consensus rules. Informational CHIPs do not necessarily represent a Bitcoin Cash community recommendation, so users and implementers are free to ignore Informational CHIPs or follow their advice.

- An Operational CHIP is a proposed change to an element of BCH that is outside of the Bitcoin Cash protocol itself. Some examples of a non protocol element include: major elements of BCH branding; the hardfork schedule; and changes to the CHIP process.

- A Technical CHIP is a proposed change to the Bitcoin Cash Protocol where full ecosystem implementation of the change is either heavily desired or required. All changes to the consensus layer must go through the Bitcoin Cash Improvement Proposal Approval Process.

### Technical CHIP Layers
Technical CHIPs are placed in one of four layers:

1. Consensus Layer  
    The consensus layer defines cryptographic commitment structures. Its purpose is ensuring that anyone can locally evaluate whether a particular state and history is valid, providing settlement guarantees, and assuring eventual convergence.  

    The consensus layer is not concerned with how messages are propagated on a network.  

    Disagreements over the consensus layer can result in network partitioning, or forks, where different nodes might end up accepting different incompatible histories. We further subdivide consensus layer changes into soft forks and hard forks.  

    In a soft fork, some structures that were valid under the old rules are no longer valid under the new rules. Structures that were invalid under the old rules continue to be invalid under the new rules.  

    In a hard fork, structures that were invalid under the old rules become valid under the new rules.

2. Peer Services Layer  
    The peer services layer specifies how nodes find each other and propagate messages.  

    Only a subset of all specified peer services are required for basic node interoperability. Nodes can support further optional extensions.  

    It is always possible to add new services without breaking compatibility with existing services, then gradually deprecate older services. In this manner, the entire network can be upgraded without serious risks of service disruption.

3. Applications Layer  
    The applications layer specifies high level structures, abstractions, and conventions that allow different applications to support similar features and share data.

4. Relay Policy Layer  
    The relay policy layer specifies the network's transaction relay and mempool policies.  

    This layer contains policies that do not have the potential to split the chain but differences between the different implementations can cause consistency/security issues for 0-conf.

### Included Information
All Chips must have a header with at least the following information

- Title:
- Created: (ISO 8601 format YYYY-MM-DD)
- Last Edited: (ISO 8601 format YYYY-MM-DD)
- Owner:
- Type: (Informational/Operational/Technical)
- Layer**: (Consensus | Peer Services | Applications | Relay Policy)
- Status: (Draft/Approved/Implemented)
- Version:

** If the CHIP is not a technical CHIP then the Layer field is not needed. Technical CHIPs should correctly identify what layer of the Bitcoin Cash protocol being affected.

All CHIPs shall, at a minimum, include the following sections. However, all sections may not be applicable. For inapplicable sections, simply indicate that it does not apply.  

- Summary
- Discussion
- Motivation and Benefits
- Description
- Specification
- Current Implementations
- Implementation Costs and Risks
- Ongoing Costs and Risks
- Evaluation of Alternatives
- Security Considerations
- Test Cases
- License
- List of Major Stakeholders
- Statements

### Document Format
All CHIPS should be a published markdown document.
The Document title should be a combination of the chip title and date in the following format:  
`CHIP-<Date>-<CHIP-Title>`

---  
## CHIP Template

```
Title:
Created:
Last Edited:
Owner:
Type:
Layer:
Status:
Version:
```

### Summary

A short description of the proposal

### Discussion
This section should Include a link(s) to where the discussion about this CHIP is taking place.

### Motivation and Benefits

This section should describe the expected benefits to BCH stakeholders from the proposal's acceptance. It is recommended that these benefits be tied to long-term BCH value.

### Description

This section should contain a definition of the proposal in more detail beyond "Summary".

A Technical CHIP should also include information such as a high-level workflow description, relevant technologies used, and the scope of expected software alterations.

### Specification

This section should contain detailed specifications of the proposal, including but not limited to protocol formats, parameter values and expected workflows. It can be updated as the CHIP makes its way through evaluation and testing, but the owner is expected to make a good-faith attempt at first publication.

### Current Implementations

This section should contain examples and proof-of-concepts of the CHIP's proposed change, or links to them. It can be updated as the CHIP makes its way through evaluation and testing.

### Implementation costs and risks

This section should list immediate costs and risks to the ecosystem if the proposal is adopted. This may include developer labor, adaptation of downstream software, risk of dissatisfaction leading to a major split, and other relevant concerns. Similar to "benefits", it is recommended that costs and risks be described in the context of their consequences to long term BCH value.

### Ongoing costs and risks

This section should list ongoing costs and risks to the ecosystem if the proposal is adopted, beyond immediate activation-time concerns. This may include node operator costs, software maintenance burden, loss of utility, loss of reputation, and so on.

### Evaluation of Alternatives

This section should describe significant alternatives to the proposal, and a brief comparison of each of their benefits/costs.

### Test Cases

This section should contain test cases that can be used to evaluate new implementations against existing ones. It can be updated as the CHIP makes its way through evaluation and testing.

### Security Considerations

This section should contain foreseeable security implications to the network. Common technical considerations include consensus risk, denial-of-service risk, and ongoing maintenance burden from additional complexity that can lead to rising likelihoods of either.

### License
This section should contain the license under which the CHIP was published. Every CHIP must be published with a public domain, permissive, or copyleft license. Creative Commons Zero (CC0), and GFDL are examples of acceptable licenses.

### List of Major Stakeholders

This section should list relevant stakeholder groups who needs to be consulted. The proposal owner should include a wide, persuasive selection from major actors in each of the groups in "Statements" below.

### Statements

This section collects comments from the above group of stakeholders, whether they are supportive, skeptical or otherwise providing relevant input. CHIP owners should be responsible for populating this section as far as possible to be persuasive to node developers and other stakeholders alike.

#### Full node developers

#### Major businesses

#### Miners and pools

#### Application developers

#### Service operators
